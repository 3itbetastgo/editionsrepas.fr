---
title: Commander par correspondance
---

<script type="module" src="{{ '/assets/bon-de-commande.js' | url }}"></script>

# Bon de commande

À imprimer puis envoyer aux éditions Repas, 4 allée Séverine 26000 Valence (France)
Tél. [04 75 42 67 45](tel:+33475426745) - mail : [nous contacter](mailto:repas@wanadoo.fr) { .masquer-pour-le-print }

Joindre votre règlement par chèque à l'ordre de l'**Association Repas**. { .masquer-pour-le-print }

{% include "bon-de-commande.njk" %}

## Adresse de livraison

<label for="order-name">Nom et prénom</label> <input type="text" id="order-name" size="20" required> { .horizontal-field }

<label for="order-postal-address">Adresse</label> <input type="text" id="order-postal-address" size="50" required> { .horizontal-field }

<label for="order-postcode">Code postal</label> <input type="text" id="order-postcode" size="6" required> { .horizontal-field }

<label for="order-town">Ville</label> <input type="text" id="order-town" size="20" required> { .horizontal-field }

<label for="order-phone">Téléphone</label> <input type="tel" id="order-phone" size="10"> { .horizontal-field }

<label for="order-email">Courriel</label> <input type="email" id="order-email" size="20"> { .horizontal-field }



-----------------
<center><small>Les données recueillies sur ce formulaire servent à faire parvenir la commande à l'adresse de livraison indiquée. Aucune commercialisation n'est faite de ces informations. Seule l'adresse de courriel, si elle est indiquée, sera ajoutée à nos fichiers informatisés dans un objectif de communication autour de nos prochaines parutions. Consultez nos mentions légales et CGV pour plus d’informations sur vos droits.</small></center>
