---
Edition: Editions REPAS
DépotLégal: 2008
Auteur: Michel Chaudy
Titre: Faire des Hommes Libres
SousTitre: Boimondau et les communautés de travail à Valence, 1941-1982
Préface: Préface de Charles Piaget
Collection: Collection Pratiques Utopiques
ISBN : 978-2-9520180-5-0
Pages: 169 pages
Prix: 15
Etat: Disponible
Résumé: |
    Voici l’histoire d’un fabricant de boîtiers de montre qui invente, en pleine Seconde Guerre mondiale, une nouvelle forme d’entreprise… Il n’est pas seulement question de fabriquer des objets et de les vendre, mais aussi de faire vivre une communauté d’hommes et de femmes qui partageront ensemble bien plus que le travail.

    C’est le début de l’aventure des communautés de travail et de la plus célèbre d’entre elles : la communauté Boimondau. Dans la foulée de celle-ci, de nombreuses autres expériences communautaires verront le jour.
Tags:
- Coopérative
- Communauté
- Travail
- Autogestion
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/faire-des-hommes-libres-michel-chaudy?_ga=2.249233159.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782952018050-faire-des-hommes-libres-boimondau-et-les-communautes-de-travail-a-valence-1941-1982-michel-chaudy/
Couverture:
Couleurs:
  Fond: '#6d92af'
  Titre: '#f4ca35'
  Texte: '#ffffff'
  PageTitres: '#e6a32b'
Adresse: 41 Rue Montplaisir, 26000 Valence
Latitude: 44.932588
Longitude: 4.907969
Vidéos:
  https://www.dailymotion.com/video/x93wpv: Sur les traces de Boimondau
  https://www.dailymotion.com/video/x93wx3: Dans le sillage de Boimondau
  https://www.youtube.com/watch?v=MJWpRxWdYaQ: "Michel Chaudy : De la prise de décision dans une communauté autogérée"
  https://www.youtube.com/watch?v=bXQ4Bzml2Hc: Faire des hommes libres - Présentation des communautés de travail - Part 1/4 (Agile France 2016)
---

## L'auteur

Militant de l'économie sociale et solidaire, syndicaliste, **Michel Chaudy** a interrogé de nombreux témoins de l'aventure de Boimondau et des communautés de travail de Valence et sa région, où lui-même habite. Il a mené par ailleurs un travail minutieux dans les archives pour restituer cette histoire méconnue.

## Un livre accompagné de vidéos

*"Sur les traces de Boimondau"* et *"Dans le sillage de Boimondau"*, les deux films que l'on trouvera sur le DVD et dans les vidéos ci-dessous, sont un voyage à travers la mémoire des anciens compagnons de cette entreprise horlogère pas comme les autres qui voulait faire du travail un outil d'émancipation collectif. En parallèle au livre de Michel Chaudy, *Faire des hommes libres*, paru aux éditions Repas, ces films laissent entendre la parole de multiples acteurs de cette histoire. Un regard subjectif, desmots sensibles, des témoignages humains qui restituent l'atmosphère unique de cette communauté de travail.

Nourris de témoignages et d'images d'archives, ces films ont été réalisés en partenariat avec Les [Brasseurs de Cages](http://brasseursdecages.free.fr/sitecontenu/pageaccueil/index.html) par Samuel Sagon.

**Sur les traces de Boimondau**    (durée 11'57")

<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" frameborder="0" type="text/html" src="https://www.dailymotion.com/embed/video/x93wpv" width="100%" height="100%" allowfullscreen > </iframe> </div>

**Dans le sillage de Boimondau**    (durée 5'50")
<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" frameborder="0" type="text/html" src="https://www.dailymotion.com/embed/video/x93wx3" width="100%" height="100%" allowfullscreen > </iframe> </div>

##  L'histoire

*Faire des hommes libres* retrace la vie des communautés de travail créées par Marcel Barbu, à Valence, à partir de 1941. En pleine guerre, voici un fabricant de boîtiers de montre qui invente une nouvelle forme d'entreprise. Il n'est pas seulement question de fabriquer des objets et de les vendre, mais aussi de faire vivre une communauté d'hommes et de femmes qui partageront ensemble bien plus que le travail.

De nombreuses expériences communautaires verront ainsi le jour. Ce livre en décrit les grandes étapes, dresse les portraits de Marcel Barbu et Marcel Mermoz, principales figures de cette aventure, raconte les difficultés de ces expériences coopératives originales et ambitieuses.

## Le commentaire des éditeurs

L'Histoire avec un grand H a trop souvent l'allure désespérante d'un rouleau compresseur, qui condamne les individus à être broyés par des forces qui les dépassent. Ainsi le travail, l'économie, la production semblent depuis deux siècles au moins avoir davantage fait des esclaves que des hommes libres…

Pourtant, des initiatives souvent marginales, mais toujours signifiantes, invitent à relativiser ce jugement sommaire. Ainsi l'expérience décrite par Michel Chaudy dans ce livre, celle des Communautés de travail qui se développèrent à Valence et dans la Drôme dans l'un des pires moments de l'histoire du XXème siècle, un de ces moments où, pour reprendre les mots de Charles Piaget dans sa préface, « *tout est à inventer.* » Et Piaget de poursuivre : « *C'est là que le livre de Michel Chaudy est important, on y trouve tout ce qui pose problème autour de ce déconditionnement de la subordination : toutes les facettes du vivre ensemble, communautés de travail ou coopérative de production ? Quelle part donner à la vie collective et quelle part à la vie individuelle ? Comment organiser le travail entre nous ? Quelle démocratie ?* ».

Dès 1944 (la première communauté de travail n'avait que trois ans d'âge), Henri Desroche et J.-L. Lebret, les pionniers d' « Economie et Humanisme », étaient déjà émerveillés par ce qu'ils découvraient dans « l'entreprise » de Marcel Barbu : « *Ce qui est frappant dans cette communauté, c'est la disparition de toute apparence sordide. La sensation de liberté, de spontanéité, d'épanouissement. L'impression d'un groupe humain en marche. Il faudrait avoir vu soi-même l'éclat des regards, saisi la netteté des questions, subi la vigueur des répliques, constaté la grande fraternité des relations, pour comprendre la valeur de cette révolution pacifique.* »

Trop beau pour être vrai ? Bien sûr, et c'est là aussi l'intérêt de la recherche présentée ici par Michel Chaudy, l'histoire des Communautés de travail a été pavée de heurts, de difficultés, d'inertie. Aussi libres qu'ils décidèrent d'être, les hommes et les femmes de Boimondau n'en demeuraient pas moins des individus comme vous et moi. On peut donc en déduire qu'inversement, vous comme moi, pouvons être acteurs d'une aventure comme celle-ci. C'est tout le bien que nous pourrions souhaiter aux lecteurs de cet ouvrage !

## Pour aller plus loin

Consultez [Le blog des Communautés de Travail](www.boimondau.fr) de Michel Chaudy.
